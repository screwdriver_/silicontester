#ifndef SILICON_HPP
# define SILICON_HPP

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

namespace Silicon
{
	using namespace std;

	struct Tag
	{
		string name;
		string file;

		inline Tag(const string& name, const string& file)
			: name{name}, file{file}
		{}
	};

	vector<Tag> get_tags();

	struct UnitTest
	{
		size_t id;
		Tag tag;

		inline UnitTest(const size_t id, const Tag& tag)
			: id{id}, tag{tag}
		{}
	};

	vector<UnitTest> get_unit_tests(const vector<Tag>& tag);

	bool compile_tests(const vector<UnitTest>& tests, const string flags);
}

#endif
